# Testing NBA framework with submodules

Submodules for C++ :
~~~bash
git submodule add https://github.com/elenavernazza/InferenceTools.git cmssw_pkgs/InferenceTools

~~~

Submodules for Python :

~~~bash
git submodule add https://gitlab.cern.ch/evernazz/analysis_tools.git python_pkgs/analysis_tools

~~~